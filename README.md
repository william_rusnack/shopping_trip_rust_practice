# Shopping Helper

**Note** Very early project and my sills have improved significantly.

Given a list of store inventories and a shopping list,
return the minimum number of store visits required to satisfy the shopping list.

See dev_data for project details
