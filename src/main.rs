extern crate serde_json as json;

extern crate itertools;
use itertools::Itertools;

use std::fs::File;
use std::collections::HashMap;

type JSONMap = json::Map<String, json::Value>;

fn convert_inventory(
  inventory_json: &JSONMap
) -> HashMap<&String, u64> {
  let mut inventory = HashMap::new();
  for (item, quantity) in inventory_json.iter() {
    inventory.insert(item, quantity.as_u64().unwrap());
  }

  inventory
}


fn main() {
  let shopping_list_path = "dev_data/shopping_list.json";
  let inventory_path = "dev_data/inventories.json";

  /* Shopping List */

  let shopping_list_file = File::open(shopping_list_path)
    .expect("shopping list file not found");

  let shopping_list_json: JSONMap = json::from_reader(shopping_list_file)
      .expect("error parsing shopping list json");

  let shopping_list = convert_inventory(&shopping_list_json);

  /* inventories */

  let stores_file = File::open(inventory_path)
    .expect("inventories file not found");

  let stores_outer_json: JSONMap = json::from_reader(stores_file)
    .expect("error parsing inventories json");
  let stores_json: &Vec<json::Value> = stores_outer_json["stores"].as_array().unwrap();

  let mut inventories = HashMap::new();
  for store in stores_json.iter() {
    let name = store["name"].as_str().unwrap();
    inventories.insert(name, convert_inventory(store["inventory"].as_object().unwrap()));
  }

  /* find what minimum store combinations have enough inventory */

  let mut inventory_totals = HashMap::new();
  for (item, _) in shopping_list.iter() {
    inventory_totals.insert(item, 0);
  }

  let mut good_combos: Vec<Vec<_>> = Vec::new();
  'combo_lens: for combo_len in 1..inventories.len() + 1 {
    'combos: for combo in inventories.iter().combinations(combo_len) {
      // reset totals to zero
      for item in shopping_list.keys() {
        *inventory_totals.get_mut(item).unwrap() = 0;
      }

      // total all values
      for &(_, inventory) in combo.iter() {
        for item in shopping_list.keys() {
          inventory.get(item).map(
            |quantity| *inventory_totals.get_mut(item).unwrap() += quantity
          );
        }
      }

      // check if there is enough inventory
      for (item, required_quantity) in shopping_list.iter() {
        if *required_quantity > inventory_totals[item] {continue 'combos;}
      }

      good_combos.push(
        combo.iter().map(|store| store.0).collect()
      );
    }

    if good_combos.len() > 0 {break 'combo_lens;}
  }

  /* report results */
  if good_combos.len() > 0 {
    println!("The shopping list can be satisfied by visiting {} store(s):", good_combos.len());
    for combo in good_combos.iter() {
      println!("{}", combo.iter().map(|s| **s).join(", "));
    }
  } else {
      println!("No combination of given stores can satisfy this shopping list :(");
  }
}
